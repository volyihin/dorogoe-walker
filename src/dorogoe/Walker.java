package dorogoe;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Node;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.google.common.base.Strings;

public class Walker {

	private static final String MAIL_SUBMIT = "mail-submit";
	private static final String MAIL_MAIL = "mail-mail";
	private static final String MAIL_PASSWORD1 = "mail-password1";
	private static final String MAIL_PASSWORD = "mail-password";
	private static final String MAIL_LOGIN = "mail-login";

	private static final String[] BOXES = new String[] { "@postalmail.biz", "@rainmail.biz",
			"@mailblog.biz" };
	public final static String MAIL = "http://api.temp-mail.ru/request/mail/id/";
	public final static String REGISTER = "http://www.dorogoe-voronezh.ru/personal/register";
	public final static String VOICE_PAGE = "http://www.dorogoe-voronezh.ru/luxury-music-lifestyle-awards-2013";
	public final static String EIGHT_PLUS = "265";
	public final static String SPORNII = "266";

	public static String getHash(String password) throws NoSuchAlgorithmException {

		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(password.getBytes());

		byte byteData[] = md.digest();

		// convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}

		// convert the byte to hex format method 2
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			String hex = Integer.toHexString(0xff & byteData[i]);
			if (hex.length() == 1)
				hexString.append('0');
			hexString.append(hex);
		}
		return hexString.toString();
	}

	public static String generateString() {
		return Double.toString(Math.random()).substring(7);
	}

	public static boolean registerDorogoe(String user, String pass, String email) {

		WebDriver driver = new HtmlUnitDriver();
		driver.get(REGISTER);
		WebElement form = driver.findElement(By.xpath("/html/body/div/div/div[3]/div[1]/div/form"));

		WebElement login = form.findElement(By.name(MAIL_LOGIN));
		WebElement password = form.findElement(By.name(MAIL_PASSWORD));
		WebElement password1 = form.findElement(By.name(MAIL_PASSWORD1));
		WebElement mail = form.findElement(By.name(MAIL_MAIL));

		WebElement submit = form.findElement(By.name(MAIL_SUBMIT));

		login.sendKeys(user);
		password.sendKeys(pass);
		password1.sendKeys(pass);
		mail.sendKeys(email);

		submit.click();
		System.out.println("Register user: " + user + " pass:" + pass + " email: " + email);
		System.out.println("Title: " + driver.getTitle());
		if (!Strings.isNullOrEmpty(driver.getTitle()))
			return true;
		else
			return false;
	}

	public static boolean generateUserAndRegister() throws InterruptedException {

		try {
			Random r = new Random();
			String boxName = generateString();
			String box = boxName + BOXES[r.nextInt(BOXES.length)];
			System.out.println(box);
			String md5Box = getHash(box);

			if (registerDorogoe(boxName, boxName, box)) {

				WebDriver driver = new HtmlUnitDriver();
				String requestEmail = MAIL + md5Box;
				System.out.println(requestEmail);
				Thread.sleep(2000);
				driver.get(requestEmail);
				String mailXml = driver.getPageSource();
				if (!Strings.isNullOrEmpty(mailXml)) {
					String registerUrl = getRegisterUrlFromXml(mailXml);
					if (!Strings.isNullOrEmpty(registerUrl)) {

						driver.get(registerUrl);
						driver.get(VOICE_PAGE);
						WebElement formElement = driver.findElement(By
								.xpath("//*[@id='llCMSPoll33']/div[2]/form"));

						System.out.println(formElement.toString());
						WebElement radioButton = formElement.findElement(By
								.xpath("//*[@id='llCMSPoll33']/div[2]/form/div[1]/div[3]/input"));
						radioButton.click();

						formElement.findElement(
								By.xpath("//*[@id='llCMSPoll33']/div[2]/form/div[5]/button"))
								.click();

						return true;

					}
				}
			}

		} catch (NoSuchAlgorithmException e) {
			System.out.println(e.getMessage());
		} catch (DocumentException e) {
			System.out.println(e.getMessage());
		}

		return false;

	}

	public static String getRegisterUrlFromXml(String mailXml) throws DocumentException {

		Document mailDocument = DocumentHelper.parseText(mailXml);
		Node node = mailDocument.selectSingleNode("xml/item/mail_text_only");
		String message = node.getStringValue().trim();
		Pattern p = Pattern.compile("\"([^\"]*)\"");
		Matcher m = p.matcher(message);
		if (m.find()) {
			return m.group(1);
		}
		return "";
	}

	public static void main(String[] args) throws InterruptedException {

		int k = 0;
		for (int i = 0; i < 5; i++) {

			if (generateUserAndRegister()) {
				k++;
			}

		}

		System.out.println("Добавлено " + k + " голосов");

	}
}
